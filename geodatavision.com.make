core = 6.x
api = 2

projects[drupal][type] = core
projects[drupal][download][type] = git
projects[drupal][download][url] = git@bitbucket.org:tommyent/geodatavision.com.git
projects[drupal][download][branch] = master
